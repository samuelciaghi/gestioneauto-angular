package marconi5bi.gestioneauto.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "proprietario")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString
public class Proprietario 
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private int id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "cognome")
    private String cognome;

    @Column(name = "telefono")
    private String telefono;

    @OneToMany(mappedBy = "proprietario", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<Auto> listaauto;
    
    // gettere e setter sul campo id
    public int getId()
    {
        return id;        
    }

    public void setId(int id)
    {
        this.id = id;
    }
    
    // gettere e setter sul campo nome
    public String getNome()
    {
        return nome;        
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }


    // gettere e setter sul campo cognome
    public String getCognome()
    {
        return cognome;        
    }

    public void setCognome(String cognome)
    {
        this.cognome = cognome;
    }


    // gettere e setter sul campo telefono
    public String getTelefono()
    {
        return telefono;        
    }

    public void setTelefono(String telefono)
    {
        this.telefono = telefono;
    }

}
