package marconi5bi.gestioneauto.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Entity
@Table(name = "auto")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString
public class Auto 
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", unique = true, nullable = false)
    private int id;

    @Column(name="targa")
    private String targa;

    @Column(name="marca")
    private String marca;

    @Column(name="modello")
    private String modello;

    @JoinColumn(name = "fkproprietario")
    @ManyToOne(cascade = CascadeType.REFRESH)
    @JsonBackReference
    private Proprietario proprietario;


    // gettere e setter sul campo id
    public int getId()
    {
        return id;        
    }

    public void setId(int id)
    {
        this.id = id;
    }


    // gettere e setter sul campo targa
    public String getTarga()
    {
        return targa;        
    }

    public void setTarga(String targa)
    {
        this.targa = targa;
    }

    // gettere e setter sul campo marca
    public String getMarca()
    {
        return marca;        
    }

    public void setMarca(String marca)
    {
        this.marca = marca;
    }

    // gettere e setter sul campo modello
    public String getModello()
    {
        return modello;        
    }

    public void setModello(String modello)
    {
        this.modello = modello;
    }
}
