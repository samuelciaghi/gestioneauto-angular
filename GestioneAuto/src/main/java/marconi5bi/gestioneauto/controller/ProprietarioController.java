package marconi5bi.gestioneauto.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import marconi5bi.gestioneauto.dto.ProprietarioAuto;
import marconi5bi.gestioneauto.entity.Proprietario;
import marconi5bi.gestioneauto.service.ProprietarioService;

@RestController
@CrossOrigin("http://localhost:4200")
public class ProprietarioController    
{

    @Autowired
    private ProprietarioService proprietarioService;

    @GetMapping("/")
    public String root() { return "Vai su:  'http://localhost:3002/getProprietari' per caricare i dati del DB.";}

    @GetMapping("/saluti")
    public String saluti()
    {
        return "\"CIAO\"";
    }


    @GetMapping(value="/getProprietari")
    public List<Proprietario> findAllProprietari()
    {
        return proprietarioService.getProprietari();
    }


    @GetMapping(value="/getProprietarioById/{id}")
    public Proprietario findProprietarioById(@PathVariable int id)
    {
        return proprietarioService.getProprietarioById(id);
    }


    @GetMapping(value="/getProprietarioByNome/{nome}")
    public Proprietario findProprietarioByNome(@PathVariable String nome)
    {
        return proprietarioService.getProprietarioByNome(nome);
    }

    
    @GetMapping(value="/getProprietarioByCognome/{cognome}")
    public Proprietario findProprietarioByCognome(@PathVariable String cognome)
    {
        return proprietarioService.getProprietarioByCognome(cognome);
    }

    
    @GetMapping(value="/getProprietariByCognome/{cognome}")
    public List<Proprietario> findProprietariByCognome(@PathVariable String cognome)
    {
        return proprietarioService.getProprietariByCognome(cognome);
    }
    
    @Transactional
    @PostMapping(value = "/addProprietario")
    public Proprietario addProprietario(@RequestBody Proprietario proprietario)
    {
        return proprietarioService.saveProprietario(proprietario);
    }

    @Transactional
    @PutMapping(value = "/updateProprietario")
    public Proprietario updateProprietario(@RequestBody Proprietario proprietario)
    {
        return proprietarioService.updateProprietario(proprietario);
    }


    @Transactional
    @DeleteMapping(value = "/deleteProprietario/{id}")
    public String deleteProprietario(@PathVariable int id)
    {
        return proprietarioService.deleteProprietario(id);
    }


    @GetMapping(value="/getInfoProprietarioInfoAutoBy/{id}")
    public List<Object[]> findInfoProprietarioInfoAuto(@PathVariable int id)
    {
        return proprietarioService.getProprietarioNCAutoMM(id);
    }

    @GetMapping(value="/getInfoProprietariInfoAuto")
    public List<ProprietarioAuto> findInfoProprietariInfoAuto()
    {
        return proprietarioService.getProprietariAutoDTO();
    }

}
