package marconi5bi.gestioneauto.controller;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import marconi5bi.gestioneauto.service.AutoService;

@RestController
public class AutoController 
{
    @Autowired
    private AutoService autoService;

    @Transactional
    @DeleteMapping(value = "/deleteAuto/{id}")
    public String deleteAuto(@PathVariable int id)
    {
        return autoService.deleteAuto(id);
    }
}
