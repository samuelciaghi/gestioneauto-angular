package marconi5bi.gestioneauto.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import marconi5bi.gestioneauto.dto.ProprietarioAuto;
import marconi5bi.gestioneauto.entity.Proprietario;
import marconi5bi.gestioneauto.repository.ProprietarioRepository;

@Service
public class ProprietarioService 
{
    @Autowired
    private ProprietarioRepository proprietarioRepository;

    public List<Proprietario> getProprietari()
    {
        return proprietarioRepository.findAll();
    }

    public Proprietario getProprietarioById(int id)
    {
        return proprietarioRepository.findById(id).orElse(null);
    }

    public Proprietario getProprietarioByNome(String nome)
    {
        return proprietarioRepository.findByNome(nome);
    }

    public Proprietario getProprietarioByCognome(String cognome)
    {
        return proprietarioRepository.findByCognome(cognome);
    }

    public List<Proprietario> getProprietariByCognome(String cognome)
    {
        return proprietarioRepository.findProprietariByCognome(cognome);
    }

    
    public Proprietario saveProprietario(Proprietario proprietario)
    {
        return proprietarioRepository.save(proprietario);
    }

    public String deleteProprietario(int id)
    {
        proprietarioRepository.deleteById(id);
        return "Eliminato proprietario con codice "+id;
    }

    public Proprietario updateProprietario(Proprietario proprietario)
    {
        Proprietario proprietarioMemorizzato = proprietarioRepository.findById(proprietario.getId()).orElse(null);

        proprietarioMemorizzato.setNome(proprietario.getNome());
        proprietarioMemorizzato.setCognome(proprietario.getCognome());
        proprietarioMemorizzato.setTelefono(proprietario.getTelefono());

        return proprietarioRepository.save(proprietarioMemorizzato);
    }


    public List<Object[]> getProprietarioNCAutoMM(int id)
    {
        return proprietarioRepository.findProprietarioNCAutoMM(id);
    }

    public List<ProprietarioAuto> getProprietariAutoDTO()
    {
        return proprietarioRepository.findProprietariAutoDTO();
    }
}
