package marconi5bi.gestioneauto.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import marconi5bi.gestioneauto.repository.AutoRepository;

@Service
public class AutoService 
{
    @Autowired
    private AutoRepository autoRepository;

    public String deleteAuto(int id)
    {
        autoRepository.deleteById(id);
        return "Eliminato auto con codice "+id;
    }    
}
