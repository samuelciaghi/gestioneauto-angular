package marconi5bi.gestioneauto.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import marconi5bi.gestioneauto.dto.ProprietarioAuto;
import marconi5bi.gestioneauto.entity.Proprietario;

@Repository
public interface ProprietarioRepository extends JpaRepository<Proprietario, Integer>
{
    public Proprietario findByNome(String nome);

    public Proprietario findByCognome(String cognome);

    @Query(value = "select * from proprietario p where p.cognome = :cognome", nativeQuery = true)
    public List<Proprietario> findProprietariByCognome(@Param("cognome") String cognome);

    @Query(value = "select 	p.nome, p.cognome, a.marca, a.modello from auto a, proprietario p where p.id = a.fkproprietario and	p.id = :id", nativeQuery = true)
    public List<Object[]> findProprietarioNCAutoMM(@Param("id") int id);

    @Query(value = "select new marconi5bi.gestioneauto.dto.ProprietarioAuto(p.nome, p.cognome, p.telefono, a.targa, a.marca, a.modello) from Auto a join a.proprietario p")
    public List<ProprietarioAuto> findProprietariAutoDTO();
}
