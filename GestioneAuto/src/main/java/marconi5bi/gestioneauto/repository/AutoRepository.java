package marconi5bi.gestioneauto.repository;

import org.springframework.stereotype.Repository;
import marconi5bi.gestioneauto.entity.Auto;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface AutoRepository extends JpaRepository<Auto, Integer>
{
    
}
