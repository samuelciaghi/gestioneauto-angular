package marconi5bi.gestioneauto.dto;

public class ProprietarioAuto {
    private String nome;
    private String cognome;
    private String telefono;
    private String targa;
    private String marca;
    private String modello;

    public ProprietarioAuto(String nome, String cognome, String telefono, String targa, String marca, String modello) {
        this.cognome = cognome;
        this.nome = nome;
        this.telefono = telefono;
        this.marca = marca;
        this.modello = modello;
        this.targa = targa;
    }

    // gettere e setter sul campo nome
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    // gettere e setter sul campo cognome
    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    // gettere e setter sul campo telefono
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    // gettere e setter sul campo targa
    public String getTarga() {
        return targa;
    }

    public void setTarga(String targa) {
        this.targa = targa;
    }

    // gettere e setter sul campo marca
    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    // gettere e setter sul campo modello
    public String getModello() {
        return modello;
    }

    public void setModello(String modello) {
        this.modello = modello;
    }

}
