import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-visualizza-dati',
  templateUrl: './visualizza-dati.component.html',
  styleUrls: ['./visualizza-dati.component.css']
})
export class VisualizzaDatiComponent implements OnInit {

  datiRicevuti: any = [];
  headers = new HttpHeaders({ 'Content-Type': 'application/json'});

  idPriorprieatio: number;
  nomeProprietario: string;
  cognomeProprietario: string;
  telefonoProprietario: any;
  autoProprieatario: any = []; //DA FINIRE


  constructor(private miohttp: HttpClient) { }

  ngOnInit(): void {
    this.getDati();

  }


  getDati() {

    this.miohttp.get('http://localhost:3002/getProprietari')
      .subscribe(dato => {
        this.datiRicevuti = dato;
        console.log('dati', this.datiRicevuti);
        let idCalcolato = this.datiRicevuti.slice(-1)[0].id;console.log(idCalcolato + 1);
      })

  }

  aggiungiProprietario() {

    let idCalcolato = this.datiRicevuti.slice(-1)[0].id + 1;

    let proprietario = {
      id: idCalcolato,
      nome: this.nomeProprietario,
      cognome: this.cognomeProprietario,
      telefono: this.telefonoProprietario,
      listaauto: this.autoProprieatario || null // DA FINIRE
    }

    this.miohttp.post('http://localhost:3002/addProprietario', proprietario)
      .subscribe(() => {
        this.getDati()
      })
  }

  eliminaProprietario(id: number) {
    this.miohttp.delete('http://localhost:3002/deleteProprietario/' + id, { headers: this.headers, responseType: 'text' })
      .subscribe(() => {
        this.getDati();
      })
  }
}
