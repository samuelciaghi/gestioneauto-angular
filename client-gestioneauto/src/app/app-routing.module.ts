import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VisualizzaDatiComponent } from './visualizza-dati/visualizza-dati.component';

const routes: Routes = [
  {path:'**', component:VisualizzaDatiComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
